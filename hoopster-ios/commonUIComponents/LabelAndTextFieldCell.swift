//
//  LabelAndTextFieldCell.swift
//  hoopster-ios
//
//  Created by Zixuan Li on 9/3/16.
//  Copyright © 2016 Hoopster. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct LabelAndTextFieldCellViewModel: CellViewModelProtocol {
    let title: String
    let textFieldPlaceholder: String
    let secureEntry: Bool
    let textOutlet: BehaviorSubject<String>
    
    init(title: String, textFieldPlaceholder: String, secureEntry: Bool, textOutlet: BehaviorSubject<String>) {
        self.title = title
        self.textFieldPlaceholder = textFieldPlaceholder
        self.secureEntry = secureEntry
        self.textOutlet = textOutlet
    }
}

class LabelAndTextFieldCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var textField: UITextField!
    
    var disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.borderStyle = .None
    }
    
    func setCellViewModel(cellViewModel: LabelAndTextFieldCellViewModel) {
        disposeBag = DisposeBag()
        label.text = cellViewModel.title
        textField.placeholder = cellViewModel.textFieldPlaceholder
        textField.secureTextEntry = cellViewModel.secureEntry
        textField.text = try? cellViewModel.textOutlet.value()
        textField.rx_text.asDriver().drive(cellViewModel.textOutlet).addDisposableTo(disposeBag)
    }
}
