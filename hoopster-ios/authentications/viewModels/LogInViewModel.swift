//
//  LogInViewModel.swift
//  hoopster-ios
//
//  Created by Zixuan Li on 9/3/16.
//  Copyright © 2016 Hoopster. All rights reserved.
//

import Foundation
import RxSwift

protocol LogInViewModelProtocol {
    var UIEvents: Observable<UIEvent> { get }
    var cellViewModels: [LabelAndTextFieldCellViewModel] { get }
}

class LogInViewModel: LogInViewModelProtocol {

    var cellViewModels = [LabelAndTextFieldCellViewModel]()
    private let UIEventsSubject = PublishSubject<UIEvent>()
    var UIEvents: Observable<UIEvent> {
        return UIEventsSubject.asObservable()
    }

    private let disposeBag = DisposeBag()
    private let emailSubject = BehaviorSubject<String>(value: "")
    private let passwordSubject = BehaviorSubject<String>(value: "")
    private var email: String = ""
    private var password: String = ""
    
    init() {
        emailSubject.subscribeNext { [unowned self] email in
            self.email = email
        }.addDisposableTo(disposeBag)
        passwordSubject.subscribeNext { [unowned self] password in
            self.password = password
        }.addDisposableTo(disposeBag)
        cellViewModels = createCellViewModels()
    }
    
    private func createCellViewModels() -> [LabelAndTextFieldCellViewModel] {
        var cellViewModels = [LabelAndTextFieldCellViewModel]()
        let emailCellViewModel = LabelAndTextFieldCellViewModel(title: "Email",
                                                           textFieldPlaceholder: "user@example.com",
                                                           secureEntry: false,
                                                           textOutlet: emailSubject)
        let passwordCellViewModel = LabelAndTextFieldCellViewModel(title: "Password",
                                                                   textFieldPlaceholder: "password",
                                                                   secureEntry: true,
                                                                   textOutlet: passwordSubject)
        cellViewModels.append(emailCellViewModel)
        cellViewModels.append(passwordCellViewModel)
        return cellViewModels
    }
    
    // Possible errors 
    // 1. email or passwords is blank
    // 2. network failure
    // 3. email or password is wrong (server-side)
    // 4. server encounters error (server-side)
    
    private func validateEmailAndPasswordArePresent(email: String, password: String) -> Bool {
        let trimmedEmail = email.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
        return trimmedEmail.characters.count > 0 && password.characters.count > 0
    }
    
    func logIn() {
        guard validateEmailAndPasswordArePresent(email, password: password) else {
            let errorMessage = NSLocalizedString("ERROR_MESSAGE_NEED_TO_FILL_REQUIRED_FIELDS", comment: "Need to fill all required fields")
            UIEventsSubject.onNext(UIEvent.DisplayErrorMessage(errorMessage))
            return
        }
        // make service call and observe on errors
    }
    
}
