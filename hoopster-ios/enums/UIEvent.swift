//
//  UIEvent.swift
//  hoopster-ios
//
//  Created by Zixuan Li on 9/5/16.
//  Copyright © 2016 Hoopster. All rights reserved.
//

import Foundation

enum UIEvent {
    case DisplayErrorMessage(String)
}