//
//  LogInViewController.swift
//  hoopster-ios
//
//  Created by Zixuan Li on 9/3/16.
//  Copyright © 2016 Hoopster. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class LogInViewController: UITableViewController {
    
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    private let cellReuseIdentifier = "LabelAndTextFieldCell"
    let disposeBag = DisposeBag()
    var logInViewModel = LogInViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.registerNib(UINib(nibName: "LabelAndTextFieldCell", bundle: NSBundle.mainBundle()), forCellReuseIdentifier: cellReuseIdentifier)
    }
    
    @IBAction func doneButtonPressed(sender: AnyObject) {
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logInViewModel.cellViewModels.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellReuseIdentifier, forIndexPath: indexPath) as!LabelAndTextFieldCell
        cell.setCellViewModel(logInViewModel.cellViewModels[indexPath.row])
        return cell
    }
    
}
