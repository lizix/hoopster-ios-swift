//
//  LogInViewModelTests.swift
//  hoopster-ios
//
//  Created by Zixuan Li on 9/5/16.
//  Copyright © 2016 Hoopster. All rights reserved.
//

@testable import hoopster_ios
import XCTest
import RxSwift

class LogInViewModelTests: XCTestCase {
    
    private let disposeBag = DisposeBag()
    private let timeout = 1.0
    var logInViewModel: LogInViewModel!
    
    override func setUp() {
        super.setUp()
        logInViewModel = LogInViewModel()
    }
    
    func testLogInWhenRequiredFieldsAreEmpty() {
        let expectation = expectationWithDescription("Should get DisplayErrorMessage UIEvent when required fields are empty")
        logInViewModel.UIEvents.subscribeNext { event in
            switch event {            case .DisplayErrorMessage(NSLocalizedString("ERROR_MESSAGE_NEED_TO_FILL_REQUIRED_FIELDS", comment: "Need to fill all required fields")):
                expectation.fulfill()
            default:
                XCTFail("should get .DisplayErrorMessage with correct associated message")
            }
        }.addDisposableTo(disposeBag)
        logInViewModel.logIn()
        waitForExpectationsWithTimeout(timeout, handler: nil)
    }

}
